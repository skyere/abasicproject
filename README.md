# Introduction 
Clean tepmlate for quick set-up the new project and it's environment

# Getting Started

To start project install Docker and run:
docker-compose -f .\docker-compose-dev.yml up --build -d

User Mobile Application UI could be found on the page:
http://localhost:2000

Postman Mobile Application application could be found on the page:
http://localhost:4000

API:
http://localhost:5000


# Build and Test


# Contribute

# Docker containers
- backend
- frontend
- postgres
- Port: UserMobile-2000, PostmanMobile-4000, BE - 5000, DB - postgres - 5432