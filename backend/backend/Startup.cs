using backend.Diagnostics;
using backend.Domain.DataContext;
using backend.Domain.Interfaces.Repisitories;
using backend.Domain.Interfaces.Services;
using backend.Domain.Repisitories;
using backend.Domain.Services;
using backend.Extensions;
using backend.Helpers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using ILogger = Serilog.ILogger;


namespace backend
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddControllers();
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                    builder =>
                    {
                        builder.AllowAnyHeader();
                        builder.AllowAnyOrigin();
                        builder.AllowAnyOrigin();
                    });
            });
            services.AddSingleton(Log.Logger);
            services.AddDbContext<ApplicationContext>(x => x.UseNpgsql(Configuration.GetValue<string>("Modules:Database:Delivery")));
            services.AddScoped<ITestService, TestService>();
            services.AddScoped<ITestRepository, TestRepository>();
            services.AddScoped<ITestCollectionsService, TestCollectionsService>();
            services.AddScoped<ITestCollectionsRepository, TestCollectionsRepository>();
            services.AddScoped<IParcelService, ParcelService>();
            services.AddScoped<IParcelRepository, ParcelRepository>();
            services.AddScoped<IUserService, UserService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger logger,
            ILoggerFactory loggerFactory, ApplicationContext applicationContext)
        {
            loggerFactory.AddSerilog();
            app.UseMiddleware<SerilogMiddleware>();

            applicationContext.Database.Migrate();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.ConfigureExceptionHandler(logger);
            app.ConfigureCustomExceptionMiddleware();


            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseCors();

            app.UseAuthorization();
            app.UseMiddleware<JwtMiddleware>();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}
