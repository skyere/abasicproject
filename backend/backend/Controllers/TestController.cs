﻿using backend.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TestController : Controller
    {
		private readonly ITestService _moviesService;
        public TestController(ITestService moviesService)
        {
            _moviesService = moviesService;
        }

        // GET: Movies
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var movies = await _moviesService.GetAsync();

            return Ok(movies);
        }

        // GET: Movies/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Movies/Create
        public ActionResult Create()
        {
            return View();
        }

        // GET: Movies/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }


        // GET: Movies/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

    }
}
