﻿using backend.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TestCollectionsController : Controller
    {
        private readonly ITestCollectionsService _testCollectionsService;
        public TestCollectionsController(ITestCollectionsService testCollectionsService)
        {
            _testCollectionsService = testCollectionsService;

        }
        // GET: Test
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var testCollections = await _testCollectionsService.GetAsync();

            return Ok(testCollections);
        }
    }
}
