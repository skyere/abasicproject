﻿using backend.Domain.DTO;
using backend.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PostmanController : Controller
    {
        private readonly IPostmanService _postmanService;
        public PostmanController(IPostmanService postmanService)
        {
            _postmanService = postmanService;

        }

        // GET: Parcels
        public async Task<IActionResult> GetAsync()
        {
            var result = await _postmanService.GetAsync();

            return Ok(result);
        }

        // GET: Parcel
        [HttpGet("{id}")]
        public async Task<IActionResult> GetByIdAsync([FromRoute] Guid id)
        {
            var result = await _postmanService.GetByIdAsync(id);

            return Ok(result);
        }

        // HttpPost: Parcel
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] ParcelRequestDTO parcel)
        {
            var result = await _postmanService.PostAsync(parcel);

            return Ok(result);
        }

        // HttpPut: Parcel
        [HttpPut]
        public async Task<IActionResult> PutAsync([FromBody] ParcelRequestDTO parcelRequest)
        {
            var result = await _postmanService.PutAsync(parcelRequest);

            return Ok(result);
        }

        // HttpDelete: Parcel
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync([FromRoute] Guid id)
        {
            var result = await _postmanService.DeleteAsync(id);

            return Ok(result);
        }
    }
}
