﻿using backend.Domain.DTO;
using backend.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParcelController : Controller
    {
        private readonly IParcelService _ParcelService;
        public ParcelController(IParcelService parcelService)
        {
            _ParcelService = parcelService;

        }

        // GET: Parcels
        public async Task<IActionResult> GetAsync()
        {
            var result = await _ParcelService.GetAsync();

            return Ok(result);
        }

        // GET: Parcel
        [HttpGet("{id:Guid}")]
        public async Task<IActionResult> GetByIdAsync([FromRoute] Guid id)
        {
            var result = await _ParcelService.GetByIdAsync(id);

            return Ok(result);
        }

        // HttpPost: Parcel
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] ParcelRequestDTO request)
        {
            var result = await _ParcelService.PostAsync(request);

            return Ok();
        }

        // HttpPut: Parcel
        [HttpPut]
        public async Task<IActionResult> PutAsync([FromBody] ParcelRequestDTO parcelRequest)
        {
            var result = await _ParcelService.PutAsync(parcelRequest);

            return Ok(result);
        }

        // HttpDelete: Parcel
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync([FromRoute] Guid id)
        {
            var result = await _ParcelService.DeleteAsync(id);

            return Ok(result);
        }
    }
}
