﻿using System;

namespace Delivery.API.Common.Models
{
    public class Test
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
