﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delivery.API.Common.Models
{
    public class PostmanParcel
    {
        public Guid Id { get; set; }
        public Guid PostmanId { get; set; }
        public Guid ParcelId { get; set; }
    }
}
