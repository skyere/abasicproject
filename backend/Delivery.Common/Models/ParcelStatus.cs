﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delivery.API.Common.Models
{
    public class ParcelStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
