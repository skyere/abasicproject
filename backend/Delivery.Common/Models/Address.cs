﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delivery.API.Common.Models
{
    public class Address
    {
        public Guid Id { get; set; }
        public string ApartmentNumber { get; set; }
        public string BuildingNumber { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Description { get; set; }
        public Guid CoordinatesId { get; set; }
    }
}
