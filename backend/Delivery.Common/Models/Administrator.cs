﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delivery.API.Common.Models
{
    public class Administrator
    {
        public Guid Id { get; set; }
        public Guid EmployeeDetailId { get; set; }
    }
}
