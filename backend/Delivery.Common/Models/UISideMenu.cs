﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delivery.API.Common.Models
{
    public class UISideMenu
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public string LinkText { get; set; }

    }
}
