﻿using System;

namespace Delivery.API.Common.Models
{
    public class TestCollection
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid TestId { get; set; }
        public string Description { get; set; }
    }
}
