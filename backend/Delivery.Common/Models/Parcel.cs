﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delivery.API.Common.Models
{
    public class Parcel
    {
        public Guid Id { get; set; }
        public Guid SenderId { get; set; }
        public Guid RecipientId { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public DateTime? HandedDate { get; set; }
        public string Description { get; set; }
        public int ParcelStatusId { get; set; }

    }
}
