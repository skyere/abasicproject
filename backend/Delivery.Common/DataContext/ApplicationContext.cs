﻿using Delivery.API.Common.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace backend.Domain.DataContext
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext()
        {

        }
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options) { }

        public DbSet<Address> Address { get; set; }
        public DbSet<AddressCoordinates> AddressCoordinates { get; set; }
        public DbSet<Administrator> Administrator { get; set; }
        public DbSet<EmployeeDetail> EmployeeDetail { get; set; }
        public DbSet<MobileSideMenu> MobileSideMenu { get; set; }
        public DbSet<Parcel> Parcel { get; set; }
        public DbSet<Postman> Postman { get; set; }
        public DbSet<PostmanParcel> PostmanParcel { get; set; }
        public DbSet<Recipient> Recipient { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<Sender> Sender { get; set; }
        public DbSet<ParcelStatus> ParcelStatus { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<UISideMenu> UISideMenu { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=postgres;Port=5432;Database=delivery;Username=postgres;Password=postgres_password;");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Parcel>()
            //.HasOne(a => a.ParcelStatus)
            //.WithOne(a => a.Parcel)
            //.HasForeignKey<ParcelStatus>(c => c.ParcelId);

            modelBuilder.Entity<Postman>().HasData(
                new Postman { Id = Guid.Parse("b87c5fc2-1ef5-49e6-991a-450dea50ca7d"), EmployeeDetailId = Guid.Parse("b87c5fc2-1ef5-49e6-991a-450dea50ca7d") },
                new Postman { Id = Guid.Parse("982ec834-80f0-4373-9ec9-7021b6b54e4e"), EmployeeDetailId = Guid.Parse("982ec834-80f0-4373-9ec9-7021b6b54e4e") }
            );

            modelBuilder.Entity<Address>().HasData(
                new Address { Id = Guid.Parse("b87c5fc2-1ef5-49e6-991a-450dea50ca7d"), Description = "Frankfurt, some address" },
                new Address { Id = Guid.Parse("982ec834-80f0-4373-9ec9-7021b6b54e4e"), Description = "Frankfurt, some address" }
            );

            modelBuilder.Entity<Recipient>().HasData(
                new Recipient { Id = Guid.Parse("c4ef80e7-2eaf-4d97-963b-f207a5892de6"), FirstName = "UserFirst 1", LastName = "UserLast 1", AddressId = Guid.Parse("b87c5fc2-1ef5-49e6-991a-450dea50ca7d") },
                new Recipient { Id = Guid.Parse("982ec834-80f0-4373-9ec9-7021b6b54e4e"), FirstName = "UserFirst 2", LastName = "UserLast 2", AddressId = Guid.Parse("982ec834-80f0-4373-9ec9-7021b6b54e4e") }
            );

            modelBuilder.Entity<PostmanParcel>().HasData(
                new PostmanParcel { Id = Guid.Parse("b87c5fc2-1ef5-49e6-991a-450dea50ca7d"), PostmanId = Guid.Parse("b87c5fc2-1ef5-49e6-991a-450dea50ca7d"), ParcelId = Guid.Parse("12f64b7a-86fa-4704-a25e-6e4b0167cbff") },
                new PostmanParcel { Id = Guid.Parse("d506ac31-c57c-4ea5-9be1-0fda41960ea1"), PostmanId = Guid.Parse("b87c5fc2-1ef5-49e6-991a-450dea50ca7d"), ParcelId = Guid.Parse("7a718cb2-dddb-4ce8-92dd-e14116c0ac2a") },
                new PostmanParcel { Id = Guid.Parse("b70058c5-1055-49ea-84bf-bfd5b26bc21f"), PostmanId = Guid.Parse("b87c5fc2-1ef5-49e6-991a-450dea50ca7d"), ParcelId = Guid.Parse("c8c474d1-9ace-4758-afe7-59295ca96d92") },
                new PostmanParcel { Id = Guid.Parse("5cd08ac6-9bd7-4f2a-9bf4-ecb12935dc92"), PostmanId = Guid.Parse("b87c5fc2-1ef5-49e6-991a-450dea50ca7d"), ParcelId = Guid.Parse("b0e77a23-8b5e-45f1-92ed-46d2ebe3a9d9") },
                new PostmanParcel { Id = Guid.Parse("6580100d-f26a-4e36-a4c9-9cb8bbe80b47"), PostmanId = Guid.Parse("982ec834-80f0-4373-9ec9-7021b6b54e4e"), ParcelId = Guid.Parse("f08597c4-bb1a-456d-bce8-d6dc57b2ec31") },
                new PostmanParcel { Id = Guid.Parse("cda42dcc-2159-4bb8-8fa1-5ce4b16328d3"), PostmanId = Guid.Parse("982ec834-80f0-4373-9ec9-7021b6b54e4e"), ParcelId = Guid.Parse("83a15891-9013-40b0-866a-78a530159a75") }
            );

            modelBuilder.Entity<ParcelStatus>().HasData(
                new ParcelStatus { Id = 1, Name = "Created"},
                new ParcelStatus { Id = 2, Name = "Received by postoffice" },
                new ParcelStatus { Id = 3, Name = "Received by postman" },
                new ParcelStatus { Id = 4, Name = "Delievered" },
                new ParcelStatus { Id = 5, Name = "Archived" }
            );

            modelBuilder.Entity<Parcel>().HasData(
                new Parcel { Id = Guid.Parse("12f64b7a-86fa-4704-a25e-6e4b0167cbff"), Description = "Parcel 1", HandedDate = new DateTime(), ParcelStatusId = 1, ReceivedDate = null, RecipientId = Guid.Parse("c4ef80e7-2eaf-4d97-963b-f207a5892de6"), SenderId = Guid.Parse("e5736e7c-6b92-4ad6-ad0c-4d26f4c54a63") },
                new Parcel { Id = Guid.Parse("7a718cb2-dddb-4ce8-92dd-e14116c0ac2a"), Description = "Parcel 2", HandedDate = new DateTime(), ParcelStatusId = 1, ReceivedDate = null, RecipientId = Guid.Parse("c4ef80e7-2eaf-4d97-963b-f207a5892de6"), SenderId = Guid.Parse("e5736e7c-6b92-4ad6-ad0c-4d26f4c54a63") },
                new Parcel { Id = Guid.Parse("c8c474d1-9ace-4758-afe7-59295ca96d92"), Description = "Parcel 3", HandedDate = new DateTime(), ParcelStatusId = 1, ReceivedDate = null, RecipientId = Guid.Parse("c4ef80e7-2eaf-4d97-963b-f207a5892de6"), SenderId = Guid.Parse("e5736e7c-6b92-4ad6-ad0c-4d26f4c54a63") },
                new Parcel { Id = Guid.Parse("b0e77a23-8b5e-45f1-92ed-46d2ebe3a9d9"), Description = "Parcel 4", HandedDate = new DateTime(), ParcelStatusId = 1, ReceivedDate = null, RecipientId = Guid.Parse("c4ef80e7-2eaf-4d97-963b-f207a5892de6"), SenderId = Guid.Parse("e5736e7c-6b92-4ad6-ad0c-4d26f4c54a63") },
                new Parcel { Id = Guid.Parse("f08597c4-bb1a-456d-bce8-d6dc57b2ec31"), Description = "Parcel 5", HandedDate = new DateTime(), ParcelStatusId = 1, ReceivedDate = null, RecipientId = Guid.Parse("c4ef80e7-2eaf-4d97-963b-f207a5892de6"), SenderId = Guid.Parse("e5736e7c-6b92-4ad6-ad0c-4d26f4c54a63") },
                new Parcel { Id = Guid.Parse("83a15891-9013-40b0-866a-78a530159a75"), Description = "Parcel 6", HandedDate = new DateTime(), ParcelStatusId = 1, ReceivedDate = null, RecipientId = Guid.Parse("c4ef80e7-2eaf-4d97-963b-f207a5892de6"), SenderId = Guid.Parse("e5736e7c-6b92-4ad6-ad0c-4d26f4c54a63") }
            );

            modelBuilder.Entity<UISideMenu>().HasData(
                new UISideMenu { Id = 1, Description = "Postmans", Link = "Postmans", LinkText = "Postmans" },
                new UISideMenu { Id = 2, Description = "Parcels", Link = "Parcels", LinkText = "Parcels" }
            );

            modelBuilder.Entity<MobileSideMenu>().HasData(
                new UISideMenu { Id = 1, Description = "Parcels", Link = "Parcels", LinkText = "Parcels" }
            );

            modelBuilder.Entity<User>().HasData(
                new User { Id = Guid.Parse("c4ef80e7-2eaf-4d97-963b-f207a5892de6"), FirstName = "John Dow", LastName = "Science Fiction", Password = "test", Username = "test" },
                new User { Id = Guid.Parse("982ec834-80f0-4373-9ec9-7021b6b54e4e"), FirstName = "Alice Cooper", LastName = "Science Fiction", Password = "test", Username = "test" },
                new User { Id = Guid.Parse("331898b1-e117-4c8c-96c2-218c1c4df378"), FirstName = "Jacques Villeneuve", LastName = "Science Fiction", Password = "test", Username = "test" },
                new User { Id = Guid.Parse("bc881bcb-6266-426c-8191-24e4d4c2020e"), FirstName = "Frank Sinatra", LastName = "Science Fiction", Password = "test", Username = "test" }
            );
        }
    }
}
