﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Delivery.API.Common.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Address",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    ApartmentNumber = table.Column<string>(type: "text", nullable: true),
                    BuildingNumber = table.Column<string>(type: "text", nullable: true),
                    Street = table.Column<string>(type: "text", nullable: true),
                    City = table.Column<string>(type: "text", nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    CoordinatesId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Address", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AddressCoordinates",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Latitude = table.Column<double>(type: "double precision", nullable: false),
                    Longitude = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AddressCoordinates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Administrator",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    EmployeeDetailId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Administrator", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeDetail",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    FirstName = table.Column<string>(type: "text", nullable: true),
                    LastName = table.Column<string>(type: "text", nullable: true),
                    RoleId = table.Column<Guid>(type: "uuid", nullable: false),
                    AddressId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeDetail", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MobileSideMenu",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Link = table.Column<string>(type: "text", nullable: true),
                    LinkText = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MobileSideMenu", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Parcel",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    SenderId = table.Column<Guid>(type: "uuid", nullable: false),
                    RecipientId = table.Column<Guid>(type: "uuid", nullable: false),
                    ReceivedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    HandedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    ParcelStatusId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Parcel", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ParcelStatus",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParcelStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Postman",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    EmployeeDetailId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Postman", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PostmanParcel",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    PostmanId = table.Column<Guid>(type: "uuid", nullable: false),
                    ParcelId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostmanParcel", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Recipient",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    FirstName = table.Column<string>(type: "text", nullable: true),
                    LastName = table.Column<string>(type: "text", nullable: true),
                    AddressId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Recipient", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sender",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    FirstName = table.Column<string>(type: "text", nullable: true),
                    LastName = table.Column<string>(type: "text", nullable: true),
                    AddressId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sender", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UISideMenu",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Link = table.Column<string>(type: "text", nullable: true),
                    LinkText = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UISideMenu", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    FirstName = table.Column<string>(type: "text", nullable: true),
                    LastName = table.Column<string>(type: "text", nullable: true),
                    Username = table.Column<string>(type: "text", nullable: true),
                    Password = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Address",
                columns: new[] { "Id", "ApartmentNumber", "BuildingNumber", "City", "CoordinatesId", "Description", "Street" },
                values: new object[,]
                {
                    { new Guid("b87c5fc2-1ef5-49e6-991a-450dea50ca7d"), null, null, null, new Guid("00000000-0000-0000-0000-000000000000"), "Frankfurt, some address", null },
                    { new Guid("982ec834-80f0-4373-9ec9-7021b6b54e4e"), null, null, null, new Guid("00000000-0000-0000-0000-000000000000"), "Frankfurt, some address", null }
                });

            migrationBuilder.InsertData(
                table: "MobileSideMenu",
                columns: new[] { "Id", "Description", "Link", "LinkText" },
                values: new object[] { 1, "Parcels", "Parcels", "Parcels" });

            migrationBuilder.InsertData(
                table: "Parcel",
                columns: new[] { "Id", "Description", "HandedDate", "ParcelStatusId", "ReceivedDate", "RecipientId", "SenderId" },
                values: new object[,]
                {
                    { new Guid("12f64b7a-86fa-4704-a25e-6e4b0167cbff"), "Parcel 1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, null, new Guid("c4ef80e7-2eaf-4d97-963b-f207a5892de6"), new Guid("e5736e7c-6b92-4ad6-ad0c-4d26f4c54a63") },
                    { new Guid("7a718cb2-dddb-4ce8-92dd-e14116c0ac2a"), "Parcel 2", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, null, new Guid("c4ef80e7-2eaf-4d97-963b-f207a5892de6"), new Guid("e5736e7c-6b92-4ad6-ad0c-4d26f4c54a63") },
                    { new Guid("c8c474d1-9ace-4758-afe7-59295ca96d92"), "Parcel 3", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, null, new Guid("c4ef80e7-2eaf-4d97-963b-f207a5892de6"), new Guid("e5736e7c-6b92-4ad6-ad0c-4d26f4c54a63") },
                    { new Guid("b0e77a23-8b5e-45f1-92ed-46d2ebe3a9d9"), "Parcel 4", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, null, new Guid("c4ef80e7-2eaf-4d97-963b-f207a5892de6"), new Guid("e5736e7c-6b92-4ad6-ad0c-4d26f4c54a63") },
                    { new Guid("f08597c4-bb1a-456d-bce8-d6dc57b2ec31"), "Parcel 5", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, null, new Guid("c4ef80e7-2eaf-4d97-963b-f207a5892de6"), new Guid("e5736e7c-6b92-4ad6-ad0c-4d26f4c54a63") },
                    { new Guid("83a15891-9013-40b0-866a-78a530159a75"), "Parcel 6", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, null, new Guid("c4ef80e7-2eaf-4d97-963b-f207a5892de6"), new Guid("e5736e7c-6b92-4ad6-ad0c-4d26f4c54a63") }
                });

            migrationBuilder.InsertData(
                table: "ParcelStatus",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Created" },
                    { 2, "Received by postoffice" },
                    { 3, "Received by postman" },
                    { 4, "Delievered" },
                    { 5, "Archived" }
                });

            migrationBuilder.InsertData(
                table: "Postman",
                columns: new[] { "Id", "EmployeeDetailId" },
                values: new object[,]
                {
                    { new Guid("982ec834-80f0-4373-9ec9-7021b6b54e4e"), new Guid("982ec834-80f0-4373-9ec9-7021b6b54e4e") },
                    { new Guid("b87c5fc2-1ef5-49e6-991a-450dea50ca7d"), new Guid("b87c5fc2-1ef5-49e6-991a-450dea50ca7d") }
                });

            migrationBuilder.InsertData(
                table: "PostmanParcel",
                columns: new[] { "Id", "ParcelId", "PostmanId" },
                values: new object[,]
                {
                    { new Guid("b87c5fc2-1ef5-49e6-991a-450dea50ca7d"), new Guid("12f64b7a-86fa-4704-a25e-6e4b0167cbff"), new Guid("b87c5fc2-1ef5-49e6-991a-450dea50ca7d") },
                    { new Guid("d506ac31-c57c-4ea5-9be1-0fda41960ea1"), new Guid("7a718cb2-dddb-4ce8-92dd-e14116c0ac2a"), new Guid("b87c5fc2-1ef5-49e6-991a-450dea50ca7d") },
                    { new Guid("b70058c5-1055-49ea-84bf-bfd5b26bc21f"), new Guid("c8c474d1-9ace-4758-afe7-59295ca96d92"), new Guid("b87c5fc2-1ef5-49e6-991a-450dea50ca7d") },
                    { new Guid("5cd08ac6-9bd7-4f2a-9bf4-ecb12935dc92"), new Guid("b0e77a23-8b5e-45f1-92ed-46d2ebe3a9d9"), new Guid("b87c5fc2-1ef5-49e6-991a-450dea50ca7d") },
                    { new Guid("6580100d-f26a-4e36-a4c9-9cb8bbe80b47"), new Guid("f08597c4-bb1a-456d-bce8-d6dc57b2ec31"), new Guid("982ec834-80f0-4373-9ec9-7021b6b54e4e") },
                    { new Guid("cda42dcc-2159-4bb8-8fa1-5ce4b16328d3"), new Guid("83a15891-9013-40b0-866a-78a530159a75"), new Guid("982ec834-80f0-4373-9ec9-7021b6b54e4e") }
                });

            migrationBuilder.InsertData(
                table: "Recipient",
                columns: new[] { "Id", "AddressId", "FirstName", "LastName" },
                values: new object[,]
                {
                    { new Guid("c4ef80e7-2eaf-4d97-963b-f207a5892de6"), new Guid("b87c5fc2-1ef5-49e6-991a-450dea50ca7d"), "UserFirst 1", "UserLast 1" },
                    { new Guid("982ec834-80f0-4373-9ec9-7021b6b54e4e"), new Guid("982ec834-80f0-4373-9ec9-7021b6b54e4e"), "UserFirst 2", "UserLast 2" }
                });

            migrationBuilder.InsertData(
                table: "UISideMenu",
                columns: new[] { "Id", "Description", "Link", "LinkText" },
                values: new object[,]
                {
                    { 1, "Postmans", "Postmans", "Postmans" },
                    { 2, "Parcels", "Parcels", "Parcels" }
                });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "FirstName", "LastName", "Password", "Username" },
                values: new object[,]
                {
                    { new Guid("e5736e7c-6b92-4ad6-ad0c-4d26f4c54a63"), "John Dow", "Science Fiction", "test", "test" },
                    { new Guid("c4ef80e7-2eaf-4d97-963b-f207a5892de6"), "Alice Cooper", "Science Fiction", "test", "test" },
                    { new Guid("331898b1-e117-4c8c-96c2-218c1c4df378"), "Jacques Villeneuve", "Science Fiction", "test", "test" },
                    { new Guid("bc881bcb-6266-426c-8191-24e4d4c2020e"), "Frank Sinatra", "Science Fiction", "test", "test" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Address");

            migrationBuilder.DropTable(
                name: "AddressCoordinates");

            migrationBuilder.DropTable(
                name: "Administrator");

            migrationBuilder.DropTable(
                name: "EmployeeDetail");

            migrationBuilder.DropTable(
                name: "MobileSideMenu");

            migrationBuilder.DropTable(
                name: "Parcel");

            migrationBuilder.DropTable(
                name: "ParcelStatus");

            migrationBuilder.DropTable(
                name: "Postman");

            migrationBuilder.DropTable(
                name: "PostmanParcel");

            migrationBuilder.DropTable(
                name: "Recipient");

            migrationBuilder.DropTable(
                name: "Role");

            migrationBuilder.DropTable(
                name: "Sender");

            migrationBuilder.DropTable(
                name: "UISideMenu");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
