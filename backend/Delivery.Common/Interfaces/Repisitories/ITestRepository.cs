﻿using Delivery.API.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace backend.Domain.Interfaces.Repisitories
{
    public interface ITestRepository
    {
        Task<IEnumerable<Test>> GetAsync();

    }
}
