﻿using backend.Domain.DTO;
using Delivery.API.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace backend.Domain.Interfaces.Repisitories
{
    public interface IParcelRepository
    {
        Task<IEnumerable<ParcelResponseDTO>> GetAsync();
        Task<ParcelResponseDTO> GetByIdAsync(Guid id);
        Task<ParcelResponseDTO> PostAsync(ParcelRequestDTO description);
        Task<ParcelResponseDTO> PutAsync(ParcelRequestDTO description);
        Task<ParcelResponseDTO> DeleteAsync();

    }
}
