﻿using backend.Domain.DTO;
using Delivery.API.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace backend.Domain.Interfaces.Repisitories
{
    public interface IPostmanRepository
    {
        Task<IEnumerable<ParcelResponseDTO>> GetAsync();
        Task<ParcelResponseDTO> GetByIdAsync(Guid id);
        Task<ParcelResponseDTO> PostAsync();
        Task<ParcelResponseDTO> PutAsync();
        Task<ParcelResponseDTO> DeleteAsync();

    }
}
