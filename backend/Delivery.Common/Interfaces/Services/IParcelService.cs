﻿using backend.Domain.DTO;
using Delivery.API.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace backend.Domain.Interfaces.Services
{
    public interface IParcelService
    {
        Task<IEnumerable<ParcelResponseDTO>> GetAsync();
        Task<ParcelResponseDTO> PostAsync(ParcelRequestDTO description);
        Task<ParcelResponseDTO> PutAsync(ParcelRequestDTO parcelRequest);
        Task<ParcelResponseDTO> DeleteAsync(Guid id);
        Task<ParcelResponseDTO> GetByIdAsync(Guid id);
    }
}
