﻿using backend.Domain.DTO;
using Delivery.API.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace backend.Domain.Interfaces.Services
{
    public interface ITestCollectionsService
    {
        Task<IEnumerable<TestCollectionResponseDTO>> GetAsync();
    }
}
