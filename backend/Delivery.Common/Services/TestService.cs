﻿using backend.Domain.Interfaces.Repisitories;
using backend.Domain.Interfaces.Services;
using Delivery.API.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace backend.Domain.Services
{
    public class TestService : ITestService
    {
        private readonly ITestRepository _moviesRepository;
        public TestService(ITestRepository moviesRepository)
        {
            _moviesRepository = moviesRepository;
        }

        public async Task<IEnumerable<Test>> GetAsync()
        {
            var result = await _moviesRepository.GetAsync();
            if (result == null)
            {
                throw new Exception();
            }

            return result;
        }
    }
}
