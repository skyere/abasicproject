﻿using backend.Domain.DTO;
using backend.Domain.Interfaces.Repisitories;
using backend.Domain.Interfaces.Services;
using Delivery.API.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace backend.Domain.Services
{
    public class TestCollectionsService : ITestCollectionsService
    {
        private readonly ITestCollectionsRepository _testCollectionsRepository;
        public TestCollectionsService(ITestCollectionsRepository testCollectionsRepository)
        {
            _testCollectionsRepository = testCollectionsRepository;
        }

        public async Task<IEnumerable<TestCollectionResponseDTO>> GetAsync()
        {
            var result = await _testCollectionsRepository.GetAsync();
            if (result == null)
            {
                throw new Exception();
            }

            return result;
        }
    }
}
