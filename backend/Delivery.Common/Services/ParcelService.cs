﻿using backend.Domain.DTO;
using backend.Domain.Interfaces.Repisitories;
using backend.Domain.Interfaces.Services;
using Delivery.API.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace backend.Domain.Services
{
    public class ParcelService : IParcelService
    {
        private readonly IParcelRepository _parcelRepository;
        public ParcelService(IParcelRepository parcelRepository)
        {
            _parcelRepository = parcelRepository;
        }

        public async Task<IEnumerable<ParcelResponseDTO>> GetAsync()
        {
            var result = await _parcelRepository.GetAsync();
            if (result == null)
            {
                throw new Exception();
            }

            return result;
        }

        public async Task<ParcelResponseDTO> GetByIdAsync(Guid id)
        {
            var result = await _parcelRepository.GetByIdAsync(id);
            if (result == null)
            {
                throw new Exception();
            }

            return result;
        }

        public Task<ParcelResponseDTO> DeleteAsync(Guid id)
        {
            throw new NotImplementedException();
        }


        public async Task<ParcelResponseDTO> PostAsync(ParcelRequestDTO description)
        {
            var result = await _parcelRepository.PostAsync(description);
            if (result == null)
            {
                throw new Exception();
            }

            return result;
        }

        public async Task<ParcelResponseDTO> PutAsync(ParcelRequestDTO parcelRequest)
        {
            var result = await _parcelRepository.PutAsync(parcelRequest);
            if (result == null)
            {
                throw new Exception();
            }

            return result;
        }
    }
}
