﻿using backend.Domain.DTO;
using backend.Domain.Interfaces.Repisitories;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace backend.Domain.Repisitories
{
    public class ParcelRepository : IParcelRepository
    {
        private IConfiguration Configuration { get; }

        public ParcelRepository(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public async Task<IEnumerable<ParcelResponseDTO>> GetAsync()
        {
            var dp = new DynamicParameters();
            var id = 1;
            dp.Add("@id", id, DbType.Int32);

            var sql = $@"SELECT * FROM public.""Parcel"" 
                WHERE public.""Parcel"".""RecipientId"" = 
				'c4ef80e7-2eaf-4d97-963b-f207a5892de6';";
            using var db = new NpgsqlConnection(Configuration.GetValue<string>("Modules:Database:Delivery"));

            return await db.QueryAsync<ParcelResponseDTO>(sql, dp);
        }

        public async Task<ParcelResponseDTO> GetByIdAsync(Guid id)
        {
            //var dp = new DynamicParameters();
            //dp.Add("@id", id, DbType.String);

            var sql = $@"SELECT public.""Parcel"".""Id"", public.""Parcel"".""Description"", public.""ParcelStatus"".""Name"" FROM public.""Parcel"" 
                JOIN public.""ParcelStatus""
                ON public.""ParcelStatus"".""Id"" = public.""Parcel"".""ParcelStatusId""
                WHERE public.""Parcel"".""Id"" = 
				'12f64b7a-86fa-4704-a25e-6e4b0167cbff';";
            using var db = new NpgsqlConnection(Configuration.GetValue<string>("Modules:Database:Delivery"));

            return await db.QuerySingleOrDefaultAsync<ParcelResponseDTO>(sql);
        }

        public async Task<ParcelResponseDTO> PostAsync(ParcelRequestDTO description)
        {
            var dp = new DynamicParameters();
            dp.Add("@description", description.Description, DbType.String);
            Guid id = Guid.NewGuid();
            dp.Add("id", id, DbType.Guid);
            var sql = $@"INSERT INTO public.""Parcel"" (""Id"", ""Description"", ""ParcelStatusId"", ""RecipientId"", ""SenderId"")
                VALUES (@id, @description, 1, 'c4ef80e7-2eaf-4d97-963b-f207a5892de6', 'e5736e7c-6b92-4ad6-ad0c-4d26f4c54a63') ";
            using var db = new NpgsqlConnection(Configuration.GetValue<string>("Modules:Database:Delivery"));

            return await db.QuerySingleOrDefaultAsync<ParcelResponseDTO>(sql, dp);
        }

        public async Task<ParcelResponseDTO> PutAsync(ParcelRequestDTO description)
        {
            var dp = new DynamicParameters();
            dp.Add("@statusId", description.StatusId, DbType.String);
            var sql = $@"UPDATE public.""Parcel"" SET ""ParcelStatusId"" = 4
                WHERE public.""Parcel"".""Id"" = 
				'12f64b7a-86fa-4704-a25e-6e4b0167cbff';";
            using var db = new NpgsqlConnection(Configuration.GetValue<string>("Modules:Database:Delivery"));

            return await db.QuerySingleOrDefaultAsync<ParcelResponseDTO>(sql, dp);
        }

        public Task<ParcelResponseDTO> DeleteAsync()
        {
            throw new System.NotImplementedException();
        }
    }
}
