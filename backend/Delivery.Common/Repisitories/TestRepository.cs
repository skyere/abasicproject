﻿using backend.Domain.Interfaces.Repisitories;
using Delivery.API.Common.Models;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace backend.Domain.Repisitories
{
    public class TestRepository : ITestRepository
    {
        private IConfiguration Configuration { get; }

        public TestRepository(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public async Task<IEnumerable<Test>> GetAsync()
        {
            var dp = new DynamicParameters();
            var id = 1;
            dp.Add("@id", id, DbType.Int32);

            var sql = $@"SELECT * FROM public.""Movie""";
            using var db = new NpgsqlConnection(Configuration.GetValue<string>("Modules:Database:Delivery"));

            return await db.QueryAsync<Test>(sql, dp);
        }
    }
}
