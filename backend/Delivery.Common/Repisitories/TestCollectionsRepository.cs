﻿using backend.Domain.DTO;
using backend.Domain.Interfaces.Repisitories;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace backend.Domain.Repisitories
{
    public class TestCollectionsRepository : ITestCollectionsRepository
    {
        private IConfiguration Configuration { get; }

        public TestCollectionsRepository(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public async Task<IEnumerable<TestCollectionResponseDTO>> GetAsync()
        {
            var dp = new DynamicParameters();
            var id = 1;
            dp.Add("@id", id, DbType.Int32);

            var sql = $@"SELECT public.""Test"".""Title"", public.""TestCollection"".""Description"", public.""User"".""FirstName"" FROM public.""TestCollection"" 
                INNER JOIN public.""User"" 
                ON public.""TestCollection"".""UserId"" = public.""User"".""Id""
                INNER JOIN public.""Test"" 
                ON public.""TestCollection"".""TestId"" = public.""Test"".""Id"" 
                ORDER BY public.""User"".""FirstName""";
            using var db = new NpgsqlConnection(Configuration.GetValue<string>("Modules:Database:Delivery"));

            return await db.QueryAsync<TestCollectionResponseDTO>(sql, dp);
        }
    }
}
