﻿using System;

namespace backend.Domain.DTO
{
    public class ParcelRequestDTO
    {
        //public Guid Id { get; set; }
        //public Guid SenderId { get; set; }
        //public Guid RecipientId { get; set; }
        //public DateTime ReceivedDate { get; set; }
        //public DateTime HandedDate { get; set; }
        //public int ParselStatusId { get; set; }
        public string Description { get; set; }
        public string StatusId { get; set; }
    }
}
