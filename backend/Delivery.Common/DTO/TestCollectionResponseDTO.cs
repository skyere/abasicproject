﻿namespace backend.Domain.DTO
{
    public class TestCollectionResponseDTO
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string FirstName { get; set; }
    }
}
