﻿using backend.Controllers;
using backend.Domain.Interfaces.Repisitories;
using Delivery.API.Common.Models;
using backend.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace backend.test
{
    public class UnitTestCollection
    {

        private List<Test> GetTestCollection()
        {
            var testMovieLists = new List<Test>()
            {
                new Test { Id = Guid.Parse("c69daee2-47a1-459c-9693-ff6dbfc463c1"), Title = "Test 1", Description = "Fiction" },
                new Test { Id = Guid.Parse("9dd6e4d7-fe3f-4004-86bb-b249ebc259d5"), Title = "Test 2", Description = "Fiction" },
                new Test { Id = Guid.Parse("92a4f462-2c69-4c04-bc60-d8cd7effaf3f"), Title = "Test 3", Description = "Fiction" },
            };
            return testMovieLists;
        }

        [Fact]
        public async Task TestService()
        {
            var mock = new Mock<ITestRepository>();
            mock.Setup(a => a.GetAsync()).ReturnsAsync(GetTestCollection());


            TestService testService = new TestService(mock.Object);

            // Act
            var actionResult = await testService.GetAsync();
            var okResult = actionResult as List<Test>;

            // Assert
            Assert.Equal(3, okResult.Count);
        }
    }
}
