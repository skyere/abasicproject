using backend.Controllers;
using Delivery.API.Common.Models;
using backend.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace backend.test
{
    public class UnitTestUsers
    {

        private List<User> GetTestUsers()
        {
            var testUserLists = new List<User>()
            {
                new User
            {
                FirstName = "FirstNameTest1",
                LastName = "LastNameTest1",
                Id = Guid.Parse("5a5f2f59-2aea-4e60-ab20-780c404e3b05"),
                Password = "testUser1pass",
                Username = "testUser1"
            },
                new User
            {
                FirstName = "FirstNameTest2",
                LastName = "LastNameTest1",
                Id = Guid.Parse("5a5f2f59-2aea-4e60-ab20-780c404e3b06"),
                Password = "testUser1pass",
                Username = "testUser1"
            }
            };
            return testUserLists;
        }

        [Fact]
        public void TestUsersController()
        {
            var mock = new Mock<IUserService>();
            mock.Setup(a => a.GetAll()).Returns(GetTestUsers());
            UsersController usersController = new UsersController(mock.Object);

            // Act
            IActionResult actionResult = usersController.GetAll();
            var okResult = actionResult as OkObjectResult;
            var actualUsers = okResult.Value;

            var actualConfigurationJStr = JsonConvert.SerializeObject(okResult.Value);
            var hereObjectWithStrongType = JsonConvert.DeserializeObject<List<User>>(actualConfigurationJStr);

            // Assert
            Assert.Equal(2, hereObjectWithStrongType.Count);
            
        }
    }
}
