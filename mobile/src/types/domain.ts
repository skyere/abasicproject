
export interface IExcelError {
    kpi: string;
    message: string;
    details: string;
}

export interface ITemplateResponse {
    message: string;
    items: IExcelError[] | string;
}
