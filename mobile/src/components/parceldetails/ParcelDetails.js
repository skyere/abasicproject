import React from 'react';
import {Link} from "react-router-dom";
import axios from 'axios';

export default class ParcelDetails extends React.Component {
    state = {
        parcel: {}
    }

    componentDidMount() {
      axios.get(`http://localhost:5000/api/parcel/12f64b7a-86fa-4704-a25e-6e4b0167cbff`)
        .then(res => {
          const parcel = res.data;
          console.log(parcel);
          this.setState({ parcel: parcel });
        });
    }

    handleSubmit = event => {
        event.preventDefault();
    
        const parcel = { parcelStatusId: 4 };
        const headers = { 
            "Content-Type": "application/json",
            'Access-Control-Allow-Origin' : 'http://localhost:4000',
            'Access-Control-Allow-Methods': 'PUT, OPTIONS',
            'Access-Control-Allow-Headers' : 'Content-Type'
        };
        axios.put('http://localhost:5000/api/parcel', parcel, headers)
            .then(response => this.setState({ description: response.data.id }));
      }
    render() {
        return (
            <div className="container home-page-container">
                <div className="row">
                    <div className="col-8 col-md-auto">
                    </div>
                </div>
                <div className="row" >
                    <div className="col justify-content-md-center d-grid gap-2 d-md-flex p-3">
                    <h3>Parcel details:</h3>
                    </div>
                    <div>
                        <ul>
                            <li>Parcel Id: {this.state.parcel.id}</li>
                            <li>Parcel Description: {this.state.parcel.description}</li>
                            <li>Parcel Status: {this.state.parcel.name}</li>
                        </ul>
                    </div>
                    <form onSubmit={this.handleSubmit}>
                        <label>
                            Change status:
                        </label>
                        <button type="submit" onSubmit={this.handleSubmit}>Delievered</button>
                        </form>

                </div>
            </div>
        )
    }
}