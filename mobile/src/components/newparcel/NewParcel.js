import React from 'react';
import {Link} from "react-router-dom";
import axios from 'axios';

export default class NewParcel extends React.Component {
    state = {
        description: 'New Parcel from UI',
    }

    handleChange = event => {
        this.setState({ description: event.target.value });
      }
    
      handleSubmit = event => {
        event.preventDefault();
    
        const parcel = {
            description: this.state.description
        };

        const article = { description: this.state.description };
        axios.post('http://localhost:5000/api/parcel', article)
            .then(response => this.setState({ description: response.data.id }));
        // axios.post(`http://localhost:5000/api/parcel`, { parcel })
        //   .then(res => {
        //     console.log(res);
        //     console.log(res.data);
        //   })
      }
  
      render() {
          return (
              <div className="container home-page-container">
                  <div className="row">
                      <div className="col-8 col-md-auto">
                      </div>
                  </div>
                  <div className="row" >
                      <div className="col justify-content-md-center d-grid gap-2 d-md-flex p-3">
                          <h3>Add parcel:</h3>
                      </div>
                      <div>
                        <form onSubmit={this.handleSubmit}>
                        <label>
                            Parcel description:
                            <input type="text" name="description" onChange={this.handleChange} />
                        </label>
                        <button type="submit">Add</button>
                        </form>
                    </div>
                  </div>
              </div>
          )
      }
}