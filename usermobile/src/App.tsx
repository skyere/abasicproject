import React from 'react';
import {hot} from 'react-hot-loader';
import './App.css';
import Footer from './components/footer/Footer';
import Header from './components/header/Header';
import { Route, Switch } from "react-router-dom";
import HomePage from "./components/homepage/HomePage";
import Parcels from "./components/parcels/Parcels";
import Postmans from "./components/postmans/Postmans";
import PageNotFound from "./components/PageNotFound";
import About from "./components/about/About";
import SideMenu from './components/sidemenu/SideMenu';
import ParcelDetails from './components/parceldetails/ParcelDetails';


const App = () => (
    <div className="app container">
      <div className='row'>
        <div className='col-3'>
          <SideMenu />
        </div>
        <div className='col-9'>
          <Header />
            <Switch>
              <Route exact path="/"  component={HomePage} />
              <Route exact path="/about" component={About} />
              <Route exact path="/homepage" component={HomePage} />
              <Route exact path="/parceldetails" component={ParcelDetails} />
              <Route exact path="/parcels" component={Parcels} />
              <Route exact path="/postmans" component={Postmans} />
              <Route component={PageNotFound} />
            </Switch>
          <Footer/> 
        </div>
      </div>
    </div>
);

export default hot(module)(App);