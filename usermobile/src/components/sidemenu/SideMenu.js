import React from 'react';

const SideMenu = () => (
    <div className=''>
    <ul>
      <li>
        <a href="Parcels" className="text-dark footer-link">
          Parcels
        </a>        
      </li>
    </ul>
  </div>
);

export default SideMenu;