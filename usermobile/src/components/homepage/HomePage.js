import React from 'react';
import {Link} from "react-router-dom";
import axios from 'axios';

export default class HomePage extends React.Component {
    state = {
        parcels: []
    }

    componentDidMount() {
      axios.get(`http://localhost:5000/api/parcel`)
        .then(res => {
          const parcels = res.data;
          this.setState({ parcels: parcels });
          console.log(parcels);
        });
    }

    render() {
        return (
            <div className="container home-page-container">
                <div className="row">
                    <div className="col-8 col-md-auto">
                    </div>
                </div>
                <div className="row" >
                    <div className="col justify-content-md-center d-grid gap-2 d-md-flex p-3">
                    <h3>Parcels:</h3>
                    </div>
                    <div>
                        <ul>
                            { this.state.parcels.map(parcels => <li>
                                <a href="ParcelDetails">{parcels.description}</a>
                                </li>)}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}