import { ITemplateResponse } from "../types/domain";
import http from "../utils/http-common";


const startProcessFile = (file: any) => {
    const formData = new FormData();
    formData.append('File', file);

    return http.post<ITemplateResponse>(`/template/`, formData,
        {
            headers: {
                'Content-Type': 'multipart/form-data'
              }
        });
};


const TutorialService = {
    startProcessFile
};

export default TutorialService;