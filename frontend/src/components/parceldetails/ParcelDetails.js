import React from 'react';
import {Link} from "react-router-dom";
import axios from 'axios';

export default class ParcelDetails extends React.Component {
    state = {
        parcel: {}
    }

    componentDidMount() {
      axios.get(`http://localhost:5000/api/parcel/12f64b7a-86fa-4704-a25e-6e4b0167cbff`)
        .then(res => {
          const parcel = res.data;
          console.log(parcel);
          this.setState({ parcel: parcel });
        });
    }

    render() {
        return (
            <div className="container home-page-container">
                <div className="row">
                    <div className="col-8 col-md-auto">
                    </div>
                </div>
                <div className="row" >
                    <div className="col justify-content-md-center d-grid gap-2 d-md-flex p-3">
                        <h3>Parcel details:</h3>
                    </div>
                    <div>
                        <ul>
                            <li>Parcel Id: {this.state.parcel.id}</li>
                            <li>Parcel Description: {this.state.parcel.description}</li>
                            <li>Parcel Status: {this.state.parcel.name}</li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}