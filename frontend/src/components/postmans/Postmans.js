import React from 'react';
import {Link} from "react-router-dom";
import axios from 'axios';

export default class Postmans extends React.Component {
    state = {
      persons: []
    }

    componentDidMount() {
      axios.get(`http://localhost:5000/api/moviecollections`)
        .then(res => {
          const persons = res.data;
          this.setState({ persons });
          console.log(persons);
        })
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-8 col-md-auto">
                    </div>
                </div>
                <div className="row" >
                    <div className="col justify-content-md-center d-grid gap-2 d-md-flex p-3">
                        <h3>Postmans</h3>
                    </div>
                    <div>
                        <ul>
                            { this.state.persons.map(person => (<li><p><b>Collection owner:</b> {person.description}, <b>Movie Title:</b> {person.title} </p></li>))}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}